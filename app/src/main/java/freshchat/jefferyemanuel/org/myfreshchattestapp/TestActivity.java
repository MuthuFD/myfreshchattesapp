package freshchat.jefferyemanuel.org.myfreshchattestapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;

/**
 * Created by muthukrishnans on 12/02/18.
 */

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Freshchat freshchat = Freshchat.getInstance(this);

        FreshchatConfig freshchatConfig = new FreshchatConfig("e176f24e-1e74-4a43-81ec-0e5a22c4ab8b", "7380fa0c-0f38-412c-8521-8208cfe4bb77");
        freshchatConfig.setGallerySelectionEnabled(true);
        freshchat.init(freshchatConfig);

        String suffix = String.valueOf(System.currentTimeMillis());

        Freshchat.resetUser(this);

        FreshchatUser freshchatUser = freshchat.getUser();
        freshchatUser.setEmail("myemail.test@host.com");
        freshchat.setUser(freshchatUser);
        freshchat.identifyUser(suffix, null);

        freshchat.showConversations(this);
    }
}
