package freshchat.jefferyemanuel.org.myfreshchattestapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.freshchat.consumer.sdk.Freshchat
import com.freshchat.consumer.sdk.FreshchatConfig
import mu.KotlinLogging
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var mChat:Freshchat
    private val logger = KotlinLogging.logger {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val freshConfig = FreshchatConfig("e176f24e-1e74-4a43-81ec-0e5a22c4ab8b", "7380fa0c-0f38-412c-8521-8208cfe4bb77")
        //freshConfig.setCameraCaptureEnabled(true);
        freshConfig.isGallerySelectionEnabled = true

        mChat = Freshchat.getInstance(this)
         val random = randomBetween(1,45000)
        mChat.init(freshConfig)
        val user = mChat.user.setEmail("myemail$random@host.com")
        mChat.user = user
        Freshchat.resetUser(this)

        mChat.identifyUser(random.toString(), null)

        listenForFreshChatRestoreId()
        Log.d ( "jeff","jeff the externalID is $random")

        Freshchat.showConversations(applicationContext)

    }

    fun randomBetween(min: Int, max: Int): Int {
        return Random().nextInt(max - min) + min
    }
    private fun listenForFreshChatRestoreId() {

        val broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val restoreId = Freshchat.getInstance(applicationContext).getUser().getRestoreId()
                Log.d ( "jeff","jeff the restoreID from broadcast is $restoreId" )
                LocalBroadcastManager.getInstance(applicationContext).unregisterReceiver(this)
            }
        }

        val intentFilter = IntentFilter(Freshchat.FRESHCHAT_USER_RESTORE_ID_GENERATED)
            LocalBroadcastManager.getInstance(applicationContext).registerReceiver(broadcastReceiver, intentFilter)


    }
}
