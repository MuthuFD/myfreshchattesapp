package freshchat.jefferyemanuel.org.myfreshchattestapp;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatUser;

/**
 * Created by muthukrishnans on 12/02/18.
 */

public class DemoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        IntentFilter intentFilter = new IntentFilter(Freshchat.FRESHCHAT_USER_RESTORE_ID_GENERATED);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            FreshchatUser freshchatUser = Freshchat.getInstance(getApplicationContext()).getUser();
            Log.d("muthu", "External id: " + freshchatUser.getExternalId());
            Log.d("muthu", "Restore id: " + freshchatUser.getRestoreId());
        }
    };

    @Override
    public void onTerminate() {
        super.onTerminate();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }
}
